$(document).ready(function () {
	var carsCatalog = $("#cars_listings").html();
	var compileData = Handlebars.compile(carsCatalog);

	$.ajax("../../db/cars.json").done(function(cars) {
		console.log(cars);
		$("#cars_container").html(compileData(cars));
	});
	$('#cars_container').on('click','#view-car-details',function (e) {
		e.preventDefault();
		console.log("Event happened");
	});
	
});