'use strict';

var gulp = require('gulp');
var concat = require('gulp-concat');
var gulp_util = require('gulp-util');
var uglify = require('gulp-uglify');
var sass = require('gulp-sass');

var server = require('gulp-connect');


var headerfooter = require('gulp-headerfooter');

gulp.task('default',['scripts','html-gen','sass','server']);

gulp.task('html-gen', function () {
  gulp.src('./content/index.html')
    .pipe(headerfooter.header('./partials/header.html'))
    .pipe(headerfooter.footer('./partials/footer.html'))
    .pipe(gulp.dest('./public/'));
});

gulp.task('scripts',function () {
	return gulp.src(['assets/js/lib/jquery-3.2.1.min.js','assets/js/lib/handlebars.js','assets/js/main.js'])
	.pipe(concat('./bundled.js'))
	.pipe(uglify())
    .pipe(gulp.dest('./dist/'));
});

gulp.task('sass',function () {
	return gulp.src('./assets/scss/**/*.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(gulp.dest('./assets/css/'));
});

gulp.task('sass:watch', function () {
  gulp.watch('./assets/scss/**/*.scss', ['sass']);
});

gulp.task('server',function () {
	server.server();
});
